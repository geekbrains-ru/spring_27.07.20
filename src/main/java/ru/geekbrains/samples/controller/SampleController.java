package ru.geekbrains.samples.controller;

import lombok.RequiredArgsConstructor;

import ru.geekbrains.samples.dao.SampleBeanDAO;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequiredArgsConstructor
public class SampleController {

	private final SampleBeanDAO sampleBeanDAO;

	@RequestMapping(value = "/sample", method = RequestMethod.GET)
	public String getSample(Model model) {
		model.addAttribute("sample", sampleBeanDAO.getText());
		return "sample";
	}

}